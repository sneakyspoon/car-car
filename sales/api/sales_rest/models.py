from django.db import models
from django.urls import reverse

class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)

class Salesperson(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    employee_id = models.CharField(max_length=100)

    def api_url(self):
        return reverse("api_salesperson", kwargs={"pk": self.pk})

    def __str__(self):
        return f"{self.last_name} - {self.first_name}/{self.employee_id}"


class Customer(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    address = models.CharField(max_length=200)
    phone_number = models.CharField(max_length=20)

    def api_url(self):
        return reverse("api_customer", kwargs={"pk": self.pk})

    def __str__(self):
        return f"{self.last_name} - {self.first_name}"

class Sale(models.Model):
    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="sales",
        on_delete=models.SET_NULL,
        null=True
    )
    salesperson = models.ForeignKey(
        Salesperson,
        related_name="sales",
        on_delete=models.SET_NULL,
        null=True
    )
    customer = models.ForeignKey(
        Customer,
        related_name="sales",
        on_delete=models.SET_NULL,
        null=True
    )
    price = models.PositiveSmallIntegerField()
