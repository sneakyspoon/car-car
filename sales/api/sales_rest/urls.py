from django.urls import path

from .views import (
    list_salespeople,
    show_salesperson,
    list_customers,
    show_customer,
    list_sales,
    delete_sale,
    list_autos,
)



urlpatterns = [
    path(
        "salespeople/",
        list_salespeople,
        name="list_salespeople"
    ),
    path(
        "salespeople/<str:employee_id>/",
        show_salesperson,
        name="show_salesperson",
    ),
    path(
        "customers/",
        list_customers,
        name="list_customers"
    ),
    path(
        "customers/<int:id>/",
        show_customer,
        name="show_customer"
    ),
    path(
        "sales/",
        list_sales,
        name="list_sales"
    ),
    path(
        "sales/<int:id>/",
        delete_sale,
        name="delete_sale"
    ),
    path(
        "automobiles/",
        list_autos,
        name="list_autos"
    )









]
