from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from .encoders import (
    SalespersonEncoder,
    CustomerEncoder,
    AutomobileVOEncoder,
    SaleEncoder,
)

from .models import Salesperson, Customer, AutomobileVO, Sale


@require_http_methods(["GET", "POST"])
def list_salespeople(request):
    if request.method == "GET":
        try:
            salespeople = Salesperson.objects.all()
            return JsonResponse(
                {"salespeople": salespeople},
                encoder=SalespersonEncoder,
            )
        except:
            response = JsonResponse(
                {"message": "Could not list salespeople"}
            )
            response.status_code = 404
            return response
    else:
        try:
            content = json.loads(request.body)
            salesperson = Salesperson.objects.create(**content)
            return JsonResponse(
                salesperson,
                encoder=SalespersonEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create Salesperson"}
            )
            response.status_code = 400
            return response

@require_http_methods(["DELETE", "GET", "PUT"])
def show_salesperson(request, employee_id):
    if request.method == "GET":
        try:
            salesperson = Salesperson.objects.get(id=employee_id)
            return JsonResponse(
                salesperson,
                encoder=SalespersonEncoder,
                safe=False
            )
        except Salesperson.DoesNotExist:
            response = JsonResponse({"message": "Salesperson does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            salesperson = Salesperson.objects.get(employee_id=employee_id)
            salesperson.delete()
            return JsonResponse(
                salesperson,
                encoder=SalespersonEncoder,
                safe=False,
            )
        except Salesperson.DoesNotExist:
            return JsonResponse({"message": "Salesperson does not exist"})
    else:
        try:
            content = json.loads(request.body)
            salesperson = Salesperson.objects.get(employee_id=employee_id)
            props = ["first_name", "last_name", "employee_id"]
            for prop in props:
                if prop in content:
                    setattr(salesperson, prop, content[prop])
            salesperson.save()
            return JsonResponse(
                salesperson,
                encoder=SalespersonEncoder,
                safe=False,
            )
        except Salesperson.DoesNotExist:
            response = JsonResponse({"message": "Salesperson does not exist"})
            response.status_code = 404
            return response

@require_http_methods(["GET", "POST"])
def list_customers(request):
    if request.method == "GET":
        try:
            customers = Customer.objects.all()
            return JsonResponse(
                {"customers": customers},
                encoder=CustomerEncoder,
            )
        except:
            response = JsonResponse(
                {"message": "Could not list customers"}
            )
            response.status_code = 404
            return response
    else:
        try:
            content = json.loads(request.body)
            customer = Customer.objects.create(**content)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create Customer"}
            )
            response.status_code = 400
            return response

@require_http_methods(["DELETE", "GET", "PUT"])
def show_customer(request, id):
    if request.method == "GET":
        try:
            customer = Customer.objects.get(id=id)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False
            )
        except Customer.DoesNotExist:
            response = JsonResponse({"message": "Customer does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            customer = Customer.objects.get(id=id)
            customer.delete()
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            response = JsonResponse(
                {"message": "Customer Does Not Exist"}
            )
            response.status_code = 404
            return response
    else:
        try:
            content = json.loads(request.body)
            customer = Customer.objects.get(id=id)
            props = ["first_name", "last_name", "address", "phone_number"]
            for prop in props:
                if prop in content:
                    setattr(customer, prop, content[prop])
            customer.save()
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            response = JsonResponse({"message": "Customer does not exist"})
            response.status_code = 404
            return response

@require_http_methods(["GET", "POST"])
def list_sales(request):
    if request.method == "GET":
        try:
            sales = Sale.objects.all()
            return JsonResponse(
                {"sales": sales},
                encoder=SaleEncoder,
            )
        except:
            response = JsonResponse(
                {"message": "Could not list sales"}
            )
            response.status_code = 404
            return response
    else:
        try:
            content = json.loads(request.body)
            auto_vo_vin = content["automobile"]
            auto = AutomobileVO.objects.get(vin=auto_vo_vin)
            content["automobile"] = auto
            auto.sold = True
            auto.save()
            salesperson_employee_id = content["salesperson"]
            salesperson = Salesperson.objects.get(employee_id=salesperson_employee_id)
            content["salesperson"] = salesperson
            customer_id = content["customer"]
            customer = Customer.objects.get(id=customer_id)
            content["customer"] = customer

            sale = Sale.objects.create(**content)
            return JsonResponse(
            sale,
            encoder=SaleEncoder,
            safe=False,
                )

        except json.JSONDecodeError:
            return JsonResponse(
                {"message": "Invalid JSON format in the request body"},
                status=400,
            )

        except:
            return JsonResponse(
                {"message": "Could not create sale"},
                status=400,
            )

@require_http_methods(["DELETE"])
def delete_sale(request, id):
    if request.method == "DELETE":
        try:
            sale = Sale.objects.get(id=id)
            sale.delete()
            return JsonResponse(
                sale,
                encoder=SaleEncoder,
                safe=False,
                )
        except Sale.DoesNotExist:
            return JsonResponse({"message": "Does not exist"}, status=404)



@require_http_methods(["GET"])
def list_autos(request):
    if request.method == "GET":
        try:
            autos = []
            all_autos = AutomobileVO.objects.all()
            for auto in all_autos:
                if auto.sold == False:
                    autos.append(auto)
            return JsonResponse(
                {"autos": autos},
                encoder=AutomobileVOEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not list autos"}
            )
            response.status_code = 404
            return response
