import React, { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'


function AutosForm() {
    const [vin, setVin] = useState('')
    const handleVinChange = (event) => {
        const value = event.target.value
        setVin(value)
    }

    const [color, setColor] = useState('')
    const handleColorChange = (event) => {
        const value = event.target.value
        setColor(value)
    }

    const [year, setYear] = useState('')
    const handleYearChange = (event) => {
        const value = event.target.value
        setYear(value)
    }

    const [models, setModels] = useState([])
    const getModels = async () => {
        const response = await fetch('http://localhost:8100/api/models/')
        if (response.ok) {
            const data = await response.json()
            setModels(data.models)
        }
    }

    const [model, setModel] = useState('')
    const handleModelChange = (event) => {
        const value = event.target.value
        setModel(value)
    }

    const navigate = useNavigate()

    const handleSubmit = async (event) => {
        event.preventDefault()
        const data = {}
        data.vin = vin
        data.color = color
        data.year = year
        data.model_id = model

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        }

        const autosUrl = 'http://localhost:8100/api/automobiles/'
        const response = await fetch(autosUrl, fetchConfig)

        if (response.ok) {
            setVin('')
            setColor('')
            setYear('')
            setModel('')
            getModels()
            navigate('/automobiles/')
        }
    }

    useEffect(() => {
        getModels()
    }, [])

    return (
        <div className="container">
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add an automobile to inventory</h1>
                    <form onSubmit={handleSubmit} id="create-automobile-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleColorChange} required type="text" id="color" name="color" className="form-control" value={color} />
                            <label htmlFor="color">Color...</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleYearChange} required type="number" id="year" name="year" className="form-control" value={year} />
                            <label htmlFor="year">Year...</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleVinChange} required type="text" id="vin" name="vin" className="form-control" value={vin} />
                            <label htmlFor="vin">Vin...</label>
                        </div>
                        <div className="mb-3">
                            <select value={model} onChange={handleModelChange} required id="model" name="model" className="form-select" >
                                <option value="">Choose a model...</option>
                                    {models.map(model => {
                                        return (
                                            <option key={model.id} value={model.id}>
                                            {model.name}
                                            </option>
                                        );
                                        })}
                            </select>
                        </div>
                    <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
        </div>
    )
}

export default AutosForm
