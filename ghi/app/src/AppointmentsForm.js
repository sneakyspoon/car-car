import React, { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'


function AppointmentsForm() {
    const [vin, setVin] = useState('')
    const handleVinChange = (event) => {
        const value = event.target.value
        setVin(value)
    }

    const [customer, setCustomer] = useState('')
    const handleCustomerChange = (event) => {
        const value = event.target.value
        setCustomer(value)
    }

    const [dateTime, setDateTime] = useState('')
    const handleDateTimeChange = (event) => {
        const value = event.target.value
        setDateTime(value)
    }

    const [technicians, setTechnicians] = useState([])
    const getTechnicians = async () => {
        const response = await fetch('http://localhost:8080/api/technicians/')
        if (response.ok) {
            const data = await response.json()
            setTechnicians(data.technicians)
        }
    }

    const [tech, setTech] = useState('')
    const handleTechChange = (event) => {
        const value = event.target.value
        setTech(value)
    }

    const [reason, setReason] = useState('')
    const handleReasonChange = (event) => {
        const value = event.target.value
        setReason(value)
    }

    const navigate = useNavigate()

    const handleSubmit = async (event) => {
        event.preventDefault()
        const data = {}
        data.vin = vin
        data.customer = customer
        data.date_time = dateTime
        data.technician = tech
        data.reason = reason

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        }

        const autosUrl = 'http://localhost:8080/api/appointments/'
        const response = await fetch(autosUrl, fetchConfig)

        if (response.ok) {
            setVin('')
            setCustomer('')
            setDateTime('')
            setTech('')
            setReason('')
            navigate('/appointments/')
        }
    }

    useEffect(() => {
        getTechnicians()
    }, [])

    return (
        <div className="container">
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a service appointment</h1>
                    <form onSubmit={handleSubmit} id="create-conference-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleVinChange} required type="text" id="vin" name="vin" className="form-control" value={vin} />
                            <label htmlFor="vin">Vin...</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleCustomerChange} required type="text" id="customer" name="customer" className="form-control" value={customer} />
                            <label htmlFor="customer">Customer...</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleDateTimeChange} required type="datetime-local" id="dateTime" name="dateTime" className="form-control" value={dateTime} />
                            <label htmlFor="dateTime">Date...</label>
                        </div>
                        <div className="mb-3">
                            <select value={tech} onChange={handleTechChange} required id="tech" name="tech" className="form-select" >
                                <option value="">Choose a technician...</option>
                                    {technicians.map(tech => {
                                        return (
                                            <option key={tech.id} value={tech.id}>
                                            {tech.first_name} {tech.last_name}
                                            </option>
                                        );
                                        })}
                            </select>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleReasonChange} required type="textarea" id="reason" name="reason" className="form-control" value={reason} />
                            <label htmlFor="reason">Reason for service...</label>
                        </div>
                    <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
        </div>
    )
}

export default AppointmentsForm
